<ul>
    <li><a href="/">Home</a></li>
    <li><a href="../loops/loops.php">Loop Demo</a></li>
    <li><a href="../countdown/ticker.php">Countdown</a></li>
    <li><a href="../magic/magic8ball.php">Magic 8 Ball</a></li>
    <li><a href="../dice/diceroller.php">Dice Roller</a></li>
    <li><a href="../oop/oop.php">OOP Demo</a></li>
    <li><a href="../movielist/movielist.php">Movie List</a></li>
    <li><a href="../customerlist/customerlist.php">Customer List</a></li>
    <li><a href="../login">Login</a></li>
    <li><a href="../api/v1/get_races">API</a></li>

</ul>