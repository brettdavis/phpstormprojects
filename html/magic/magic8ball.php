<?php
    session_start();

    //print_r($_SESSION);

    $question = ' ';
    if(isset($_POST['txt_question'])){
        $question = $_POST['txt_question'];
    }

    $prev_question = ' ';
    if(isset($_SESSION['prev_question'])){
        $prev_question = $_SESSION['prev_question'];
    }
    //print_r($question);

    $responses[0] = 'Ask again later';
    $responses[1] = 'Yes';
    $responses[2] = 'No';
    $responses[3] = 'It appears to be';
    $responses[4] = 'Reply is hazy, please try again';
    $responses[5] = 'Yes, definitely';
    $responses[6] = 'What is it you really want to know?';
    $responses[7] = 'Outlook is good';
    $responses[8] = 'My sources say no';
    $responses[9] = 'Signs point to yes';
    $responses[10] = 'Dont count on it';
    $responses[11] = 'Cannot predict now';
    $responses[12] = 'As I see it, Yes';
    $responses[13] = 'Better not tell you now';
    $responses[14] = 'Concentrate and ask again';

    if($question == ' '){
        $answer = "Ask Me A Question";
    }else if(substr($question, -1) != '?'){
        $answer = "Ask Me A Question (use a question mark)";
    }else if($question == $prev_question){
        $answer = 'Please Ask A Different Question';
    }else{
        $index_response = mt_rand(0, 14);
        $answer = $responses[$index_response];
        $_SESSION['prev_question'] = $question;
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Magic 8 Ball</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include('../includes/header.php'); ?></header>
<nav><?php include ('../includes/nav.php'); ?></nav>
<main>
    <h2>Magic 8 ball</h2>

    <marquee><?=$answer?></marquee>

    <form action ="" method="post">
        <label for="txt_question">Question</label>
        <input
            type="text"
            name="txt_question"
            id="txt_question"
            value="<?=$question?>"
            placeholder="Will I pass this class?"/>
        <input type="submit" value="Ask Your Question"/>
    </form>
</main>
<footer><?php include ('../includes/footer.php'); ?></footer>
</body>
</html>