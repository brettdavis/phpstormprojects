<?php
include_once ('../includes/dbConfig.php');
if(isset($_GET['custid']) && !empty($_GET['custid']))
{
    $custid = $_GET['custid'];

    try{
        $db = new PDO($dsn, $username, $password, $options);

        $sql = $db->prepare("SELECT * FROM phpclass.customerlist WHERE custid = :Custid");
        $sql->bindValue(':Custid', $custid);
        $sql->execute();
        $row = $sql->fetch();

        $fname = $row['firstname'];
        $lname = $row['lastname'];
        $line1 = $row['line1'];
        $line2 = $row['line2'];
        $city = $row['city'];
        $state = $row['state'];
        $zipcode = $row['zip'];
        $phone = $row['phone'];
        $email = $row['email'];
        $pass = $row['pass'];

    }catch(PDOException $e){
        $error = $e->getMessage();
        echo "Error: ".$error;
        exit();
    }
}
//Checking for all the inputs
if(
    isset($_POST['firstname']) && !empty($_POST['firstname'])
    && isset($_POST['lastname']) && !empty($_POST['lastname'])
    && isset($_POST['line1']) && !empty($_POST['line1'])
    && isset($_POST['city']) && !empty($_POST['city'])
    && isset($_POST['state']) && !empty($_POST['state'])
    && isset($_POST['zip']) && !empty($_POST['zip'])
    && isset($_POST['phone']) && !empty($_POST['phone'])
    && isset($_POST['email']) && !empty($_POST['email'])
    && isset($_POST['pass']) && !empty($_POST['pass'])
)
{
    //var_dump($_POST); exit;
    $custid = $_POST['custid'];
    $fname = $_POST['firstname'];
    $lname = $_POST['lastname'];
    $line1 = $_POST['line1'];
    $line2 = $_POST['line2'];
    $city = $_POST['city'];
    $state = $_POST['state'];
    $zipcode = $_POST['zip'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $pass = $_POST['pass'];

    try {
        $db = new PDO($dsn, $username, $password, $options);
        $sql = $db->prepare("
                              UPDATE phpclass.customerlist
                              SET 
                              firstname = :Fname, lastname = :Lname , line1 = :Line1, line2 = :Line2 , city = :City, state = :State , 
                              zip = :Zipcode, phone = :Phone , email = :Email, pass = :Pass 
                              WHERE custid = :Custid");
        $sql->bindValue(':Custid', $custid);
        $sql->bindValue(':Fname', $fname);
        $sql->bindValue(':Lname', $lname);
        $sql->bindValue(':Line1', $line1);
        $sql->bindValue(':Line2', $line2);
        $sql->bindValue(':City', $city);
        $sql->bindValue(':State', $state);
        $sql->bindValue(':Zipcode', $zipcode);
        $sql->bindValue(':Phone', $phone);
        $sql->bindValue(':Email', $email);
        $sql->bindValue(':Pass', $pass);
        $sql->execute();
    } catch (PDOException $e) {
        echo "DB Error: " . $e->getMessage();
        exit;
    }
    header("Location:customerlist.php?success=1");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Update Customer</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>
<header><?php include('../includes/header.php'); ?></header>
<nav><?php include ('../includes/nav.php'); ?></nav>
<main>
    <div class="row">
        <div class="col-md-6">
            <form action="" method="post">
                <input type="hidden" name="custid" value="<?= $custid ?>">
                <table class="table table-hover table-border"style="margin: auto;">
                    <tr>
                        <th colspan="2"><h3>Update a Customer</h3></th>
                    </tr>
                    <tr>
                        <th>First Name</th>
                        <td><input type="text" name="firstname" value="<?=$fname?>" size="50" required></td>
                    </tr>
                    <tr>
                        <th>Last Name</th>
                        <td><input type="text" name="lastname" value="<?=$lname?>" size="50" required></td>
                    </tr>
                    <tr>
                        <th>Address Line 1</th>
                        <td><input type="text" name="line1" value="<?=$line1?>" size="50" required></td>
                    </tr>
                    <tr>
                        <th>Address Line 2</th>
                        <td><input type="text" name="line2" value="<?=$line2?>" size="50"></td>
                    </tr>
                    <tr>
                        <th>City</th>
                        <td><input type="text" name="city" value="<?=$city?>" size="50" required></td>
                    </tr>
                    <tr>
                        <th>State</th>
                        <td><input type="text" name="state" value="<?=$state?>" size="2" required></td>
                    </tr>
                    <tr>
                        <th>Zipcode</th>
                        <td><input type="text" name="zip" value="<?=$zipcode?>" pattern="[0-9]{5}" required></td>
                    </tr>
                    <tr>
                        <th>Phone Number</th>
                        <td><input type="tel" name="phone" value="<?=$phone?>" size="50" required></td>
                    </tr>
                    <tr>
                        <th>Email</th>
                        <td><input type="email" name="email" value="<?=$email?>" size="50" required></td>
                    </tr>
                    <tr>
                        <th>Password</th>
                        <td><input type="password" name="pass" value="<?=$pass?>" size="50" required></td>
                    </tr>
                    <tr>
                        <td><input class="btn btn-primary" type="submit" value="Update"></td>
                        <td><input class="btn btn-danger" type="button" value="Delete Customer" onclick="deleteCustomer('<?=$custid?>')"></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</main>
<footer><?php include ('../includes/footer.php'); ?></footer>
<script type="text/javascript">
    function deleteCustomer(custid)
    {
        if(confirm("Do you want to delete this customer?"))
        {
            document.location.href = "customerdelete.php?custid=" + custid;
        }
    }
</script>
</body>
</html>