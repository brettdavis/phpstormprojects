<?php
include_once ('../includes/dbConfig.php');
try{
    $custid = $_GET['custid'];
    $db = new PDO($dsn, $username, $password, $options);
    $sql = $db->prepare("SELECT * FROM phpclass.customerlist");
    $sql->execute();
    $rows = $sql->fetchAll();
}catch(PDOException $e){
    $error = $e->getMessage();
    echo "Error: ".$error;
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Customer List</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include('../includes/header.php'); ?></header>
<nav><?php include ('../includes/nav.php'); ?></nav>
<main>
    <h3>Customer List</h3>
    <table border="1" style="border-collapse: collapse; margin: auto; width: 80%;">
        <tr>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Address Line 1</th>
            <th>Address Line 2</th>
            <th>City</th>
            <th>State</th>
            <th>Zipcode</th>
            <th>Phone Number</th>
            <th>Email</th>
            <th>Password</th>
        </tr>
        <?php foreach($rows as $row): ?>
            <tr>
                <td><a href="customerupdate.php?custid=<?=$row['custid']?>"><?=$row['custid']?></a></td>
                <td><?= $row['firstname']?></td>
                <td><?= $row['lastname']?></td>
                <td><?= $row['line1']?></td>
                <td><?= $row['line2']?></td>
                <td><?= $row['city']?></td>
                <td><?= $row['state']?></td>
                <td><?= $row['zip']?></td>
                <td><?= $row['phone']?></td>
                <td><?= $row['email']?></td>
                <td><?= $row['pass']?></td>
            </tr>
        <?php endforeach;?>
    </table>
    <br>
    <input type="button" value="Add Customer" onclick="addMovie('<?=$custid?>')">
</main>
<footer><?php include ('../includes/footer.php'); ?></footer>
<script type="text/javascript">
    function addMovie(custid)
    {
            document.location.href = "customeradd.php?custid=" + custid;
    }
</script>
</body>
</html>