<?php
include_once ('../includes/dbConfig.php');
if(isset($_GET['custid']) && !empty($_GET['custid']))
{
    $custid = $_GET['custid'];

    try{
        $db = new PDO($dsn, $username, $password, $options);

        $sql = $db->prepare("DELETE FROM phpclass.customerlist WHERE custid = :Custid");
        $sql->bindValue(':Custid', $custid);
        $sql->execute();
        header("Location:customerlist.php?success=1");
    }catch(PDOException $e) {
        $error = $e->getMessage();
        echo "Error: " . $error;
        exit();
    }
}
header("Location:customerlist.php");