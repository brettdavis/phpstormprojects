<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Run Master - Add Marathon</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?= asset_url(); ?>css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?= asset_url(); ?>css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?= asset_url(); ?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <?php
            $this->load->view('includes/header');
            $this->load->view('includes/menu');
            ?>
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12">

                        <form method="post" action="/site/admin/add_race" role="form">

                            <div class="form-group">
                                <h2>Add a Race</h2>
                                <label>Race Name</label>
                                <input name="txt_name" type="text" class="form-control">
                                <p class="help-block">Name of the race.</p>
                            </div>

                            <div class="form-group">
                                <label>Race Location</label>
                                <input name="txt_location" type="text" class="form-control">
                                <p class="help-block">Location of the race.</p>
                            </div>

                            <div class="form-group">
                                <label>Race Description</label>
                                <textarea name="txt_description" class="form-control" rows="3"></textarea>
                                <p class="help-block">Description of the race.</p>
                            </div>

                            <div class="form-group">
                                <label>Race Date and Time</label>
                                <input name="txt_date" type="text" class="form-control">
                                <p class="help-block">Format: YYY-MM-DD HH:MM:SS</p>
                            </div>

                            <button type="submit" class="btn btn-default">Add Race</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?= asset_url(); ?>js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?= asset_url(); ?>js/bootstrap.min.js"></script>

</body>

</html>
