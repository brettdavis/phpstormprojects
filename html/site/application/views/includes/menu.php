<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
<div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav side-nav">
        <li <?= isset($dashboard) ? 'class = "active"' : '' ?>>
            <a href="/site/admin"><i class="fa fa-fw fa-table"></i> Dashboard</a>
        </li>
        <li <?= isset($manage_marathon) ? 'class = "active"' : '' ?>>
            <a href="/site/admin/manage_marathon"><i class="fa fa-fw fa-wrench"></i> Manage Marathon</a>
        </li>
        <li <?= isset($add_marathon) ? 'class = "active"' : '' ?>>
            <a href="/site/admin/add_marathon"><i class="fa fa-fw fa-edit"></i> Add Marathon</a>
        </li>
        <li <?= isset($manage_runners) ? 'class = "active"' : '' ?>>
            <a href="/site/admin/manage_runners"><i class="fa fa-fw fa-wrench"></i> Manage Runners</a>
        </li>
        <li <?= isset($registration_form) ? 'class = "active"' : '' ?>>
            <a href="/site/admin/registration_form"><i class="fa fa-fw fa-file"></i> Registration Form</a>
        </li>
    </ul>
</div>
<!-- /.navbar-collapse -->