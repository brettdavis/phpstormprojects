<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    /*
	 * Dashboard = T7
	 * Manage Marathon = T7
	 * Add Marathon = T5
	 * Manage Runners = T7
	 * Registration Form = T2
	 */

	//Dashboard
	public function index()
	{
        $data = ['dashboard' => true];
		$this->load->view('public/admin/dashboard', $data);
	}

    public function manage_marathon()
    {
        $this->load->model('Race');
        $data = [
            'manage_marathon' => true,
            'races'=>$this->Race->get_races()
        ];
        $this->load->view('public/admin/manage_marathon', $data);
    }

    public function add_marathon()
    {

        $data = [
            'add_marathon' => true
        ];
        $this->load->view('public/admin/add_marathon', $data);
    }

    public function manage_runners()
    {
        $data = ['manage_runners' => true];
        $this->load->view('public/admin/manage_runners', $data);
    }

    public function registration_form()
    {
        $data = ['registration_form' => true];
        $this->load->view('public/admin/registration_form', $data);
    }

    public function add_race()
    {
        $this->load->model('Race');
        $this->Race->add_race(
            $this->input->post('txt_name'),
            $this->input->post('txt_location'),
            $this->input->post('txt_description'),
            $this->input->post('txt_date')
        );

        redirect('admin/manage_marathon', 'refresh');
    }

    public function delete_race($id)
    {
        $this->load->model('Race');
        $this->Race->delete_race($id);
        redirect('admin/manage_marathon', 'refresh');
    }

    public function update_race($id)
    {
        $this->load->model('Race');
        $data = ['race'=>$this->Race->get_race($id)];
        $this->load->view('public/admin/update_marathon', $data);
    }

    public  function update_race_post()
    {
        $this->load->model('Race');

        $this->Race->update_race(
            $this->input->post('txt_name'),
            $this->input->post('txt_location'),
            $this->input->post('txt_description'),
            $this->input->post('txt_date'),
            $this->input->post('txt_id')
        );

        redirect('admin/manage_marathon', 'refresh');
    }

    /*Demo Templates
    public function view_1()
    {
        $this->load->view('public/admin/1');
    }

    public function view_2()
    {
        $this->load->view('public/admin/2');
    }

    public function view_3()
    {
        $this->load->view('public/admin/3');
    }

    public function view_4()
    {
        $this->load->view('public/admin/4');
    }

    public function view_5()
    {
        $this->load->view('public/admin/5');
    }

    public function view_6()
    {
        $this->load->view('public/admin/6');
    }

    public function view_7()
    {
        $this->load->view('public/admin/7');
    }
    */
}
