<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Model {

    public function user_login($email, $pwd){

        $this->load->database();
        $this->load->library('session');

        try {
            //connect to the database
            $db = new PDO($this->db->dsn, $this->db->username, $this->db->password, $this->db->options);
            $sql = $db->prepare("
                          SELECT 
                            password,  member_key 
                          FROM 
                            phpclass.member_login 
                          WHERE 
                            email = :Email 
                            AND role_id = 3");
            $sql->bindValue(':Email', $email);
            $sql->execute();
            $row = $sql->fetch();

            if ($row != null) {
                $hashed_password = md5($pwd . $row['member_key']);

                //password check for promoter
                if ($hashed_password == $row['password']) {
                    $this->session->set_userdata(['UID' => $row['member_key']]);
                    //$_SESSION['UID'] = $row['member_key'];

                    return true;
                }else {
                   return false;
                }
            } else {
                return false;
            }
        }catch(PDOException $e){
            //print_r($e); exit;
            return false;
        }

    }

    public function user_create($name, $email, $pwd)
    {
        try {

            $this->load->database();
            $this->load->library('session');

            $member_key = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
            $hashed_password = md5($pwd . $member_key);

            //connect to the database
            $db = new PDO($this->db->dsn, $this->db->username, $this->db->password, $this->db->options);
            $sql = $db->prepare("
                          INSERT INTO 
                            phpclass.member_login
                            (name, email, password, role_id, member_key) 
                          VALUES
                            (:Name , :Email, :Password, 3, :Memberkey)");
            $sql->bindValue(':Name', $name);
            $sql->bindValue(':Email', $email);
            $sql->bindValue(':Password', $hashed_password);
            $sql->bindValue(':Memberkey', $member_key);
            $sql->execute();
            return true;
        } catch (PDOException $e) {
            return false;
        }
    }
}
