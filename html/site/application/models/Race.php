<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Race extends CI_Model {

    /**
     * Get races from phpclass.race table
     *
     * @return mixed should be an array list of races
     */
    public function get_races(){

        $this->load->database();

        try{
            $query = $this->db->get("race");
            return $query->result_array();

        }catch(PDOException $e){
            echo "DB Error: ".$e.Message();exit;
        }

    }

    /**
     * Add a new race
     * @param string $name
     * @param string $location
     * @param string $description
     * @param string $date  Format: YYY-MM-DD HH:MM:SS
     */
    public function add_race($name, $location, $description, $date)
    {
        $this->load->database();

        try{
            $data = [
                'race_name' => $name,
                'race_location' => $location,
                'race_description' => $description,
                'race_date_time' => $date
            ];
            $query = $this->db->insert("race",$data);

        }catch(PDOException $e){
            echo "DB Error: ".$e.Message();exit;
        }
    }

    public function delete_race($id)
    {
        $this->load->database();

        try {
            $data = ['race_id' => $id];
            $query = $this->db->delete("race",$data);

        } catch(PDOException $e){
            echo "DB Error: ".$e.Message();exit;
        }
    }

    /**
     * Delete the race
     *
     * @param $id race_id from race table
     * @return mixed
     */
    public function get_race($id){

        $this->load->database();

        try{
            $data = ['race_id'=>$id];
            $query = $this->db->get_where("race", $data);
            return $query->result_array();

        }catch(PDOException $e){
            echo "DB Error: ".$e.Message();exit;
        }
    }

    /**
     * Update a given race by race_id
     *
     * @param string $name
     * @param string  $location
     * @param string $description
     * @param string $date Format: YYY-MM-DD HH:MM:SS
     * @param int $id phpclass.race primary key (race_id)
     */
    public function update_race($name, $location, $description, $date, $id)
    {
        $this->load->database();

        try{
            $data = [
                'race_name' => $name,
                'race_location' => $location,
                'race_description' => $description,
                'race_date_time' => $date,
            ];
            $this->db->where('race_id', $id);
            $query = $this->db->update("race",$data);

        }catch(PDOException $e){
            echo "DB Error: ".$e.Message();exit;
        }
    }
}