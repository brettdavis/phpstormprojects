<?php
//Checking for both the title and rating
if(
    isset($_POST['movietitle']) && !empty($_POST['movietitle'])
    && isset($_POST['movierating']) && !empty($_POST['movierating']))
{
    //var_dump($_POST); exit;
    $title = $_POST['movietitle'];
    $rating = $_POST['movierating'];

    //dbstuff
    include ('../includes/dbConfig.php');
    try
    {
        $db = new PDO($dsn, $username, $password, $options);
        $sql = $db->prepare("
                              INSERT INTO phpclass.movielist
                              (name, rating) 
                              VALUE 
                              (:Title , :Rating)");
        $sql->bindValue(':Title', $title);
        $sql->bindValue(':Rating', $rating);
        $sql->execute();
    }
    catch (PDOException $e)
    {
        echo "DB Error: ". $e->getMessage();
        exit;
    }
    //exit('DB write');
    header("Location:movielist.php?success=1");
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Add Movie</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>
<header><?php include('../includes/header.php'); ?></header>
<nav><?php include ('../includes/nav.php'); ?></nav>
<main>
    <h3>Add a Movie</h3>
    <div class="row">
        <div class="col-md-6">
    <form action="" method="post">
        <table class="table table-hover table-border"style="margin: auto;">
            <tr>
                <th colspan="2"><h3>Add a Movie</h3></th>
            </tr>
            <tr>
                <th>Title</th>
                <td><input type="text" name="movietitle" value="" size="50" required></td>
            </tr>
            <tr>
                <th>Rating</th>
                <td><input type="text" name="movierating" value="" size="50" required></td>
            </tr>
            <tr>
               <th colspan="2"><input type="submit" value="submit"></th>
            </tr>
        </table>
    </form>
        </div>
    </div>
</main>
<footer><?php include ('../includes/footer.php'); ?></footer>
</body>
</html>