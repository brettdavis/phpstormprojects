<?php
include_once ('../includes/dbConfig.php');
try{
$db = new PDO($dsn, $username, $password, $options);
$sql = $db->prepare("SELECT * FROM phpclass.movielist ORDER BY name");
$sql->execute();
$rows = $sql->fetchAll();
}catch(PDOException $e){
    $error = $e->getMessage();
    echo "Error: ".$error;
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Movie List</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include('../includes/header.php'); ?></header>
<nav><?php include ('../includes/nav.php'); ?></nav>
<main>
    <h3>Movie List</h3>
    <table border="1" style="border-collapse: collapse; margin: auto; width: 80%;">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Rating</th>
        </tr>
        <?php foreach($rows as $row): ?>
            <tr>
                <td><?= $row['id']?></td>
                <td><a href="movieupdate.php?id=<?= $row['id'] ?>"><?= $row['name']?></a></td>
                <td><?= $row['rating']?></td>
            </tr>
        <?php endforeach;?>
    </table>
    <br>
    <p style="font-size: 1.5rem; margin-left:auto; margin-right:auto;"><a href="movieadd.php" target="_self">Add a Movie</a></p>
</main>
<footer><?php include ('../includes/footer.php'); ?></footer>
</body>
</html>
