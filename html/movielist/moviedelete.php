<?php
include_once ('../includes/dbConfig.php');
if(isset($_GET['id']) && !empty($_GET['id']))
{
    $id = $_GET['id'];

    try{
        $db = new PDO($dsn, $username, $password, $options);

        $sql = $db->prepare("DELETE FROM phpclass.movielist WHERE id = :Id");
        $sql->bindValue(':Id', $id);
        $sql->execute();
        header("Location:movielist.php?success=1");
    }catch(PDOException $e) {
        $error = $e->getMessage();
        echo "Error: " . $error;
        exit();
    }
}
header("Location:movielist.php");
