<?php
include_once ('../includes/dbConfig.php');
if(isset($_GET['id']) && !empty($_GET['id']))
{
    $id = $_GET['id'];

    try{
        $db = new PDO($dsn, $username, $password, $options);

        $sql = $db->prepare("SELECT * FROM phpclass.movielist WHERE id = :Id");
        $sql->bindValue(':Id', $id);
        $sql->execute();
        $rows = $sql->fetch();

        $title = $rows['name'];
        $rating = $rows['rating'];

    }catch(PDOException $e){
        $error = $e->getMessage();
        echo "Error: ".$error;
        exit();
    }
}

//Checking for both the title and rating
if(
    isset($_POST['movieid']) && !empty($_POST['movieid'])
    && isset($_POST['movietitle']) && !empty($_POST['movietitle'])
    && isset($_POST['movierating']) && !empty($_POST['movierating']))
{
    //var_dump($_POST); exit;
    $id = $_POST['movieid'];
    $title = $_POST['movietitle'];
    $rating = $_POST['movierating'];

    try
    {
        $db = new PDO($dsn, $username, $password, $options);
        $sql = $db->prepare("
                              UPDATE phpclass.movielist
                              SET name = :Name, rating = :Rating 
                              WHERE id = :Id");
        $sql->bindValue(':Name', $title);
        $sql->bindValue(':Rating', $rating);
        $sql->bindValue(':Id', $id);
        $sql->execute();
    }
    catch (PDOException $e)
    {
        echo "DB Error: ". $e->getMessage();
        exit;
    }
    header("Location:movielist.php?success=1");
}


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Update Movie</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>
<header><?php include('../includes/header.php'); ?></header>
<nav><?php include ('../includes/nav.php'); ?></nav>
<main>
    <h3>Update a Movie</h3>
    <div class="row">
        <div class="col-md-6">
            <form action="" method="post">
                <input type="hidden" name="movieid" value="<?= $id ?>">
                <table class="table table-hover table-border" style="margin: auto;">
                    <tr>
                        <th><h3>Update a Movie</h3></th>
                    </tr>
                    <tr>
                        <th>Title</th>
                        <td><input type="text" name="movietitle" value="<?=$title?>" size="50" required></td>
                    </tr>
                    <tr>
                        <th>Rating</th>
                        <td><input type="text" name="movierating" value="<?=$rating?>" size="50" required></td>
                    </tr>
                    <tr>
                        <td><input class="btn btn-primary" type="submit" value="Update"></td>
                        <td><input class="btn btn-danger" type="button" value="Delete Movie" onclick="deleteMovie('<?=$title?>', '<?=$id?>')"></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</main>
<footer><?php include ('../includes/footer.php'); ?></footer>
<script type="text/javascript">
    function deleteMovie(title, id)
    {
        if(confirm("Do you want to delete" + title + "?"))
        {
            document.location.href = "moviedelete.php?id=" + id;
        }
    }
</script>
</body>
</html>