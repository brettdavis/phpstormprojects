<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Loops</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include('../includes/header.php'); ?></header>
<nav><?php include ('../includes/nav.php'); ?></nav>
<main style="text-align: center;">

    <?php
        $number = 100;
        //print($number);
        //echo"<h1>".$number."</h1>";
        //echo "<h1>{$number}</h1>";
        //$result = "<h1>";
        //$result .= $number;
        //$result .= "</h1>";
        //echo $result;
        //echo '<h1>$number</h1>' showed the literal $number as an h1
        $num1 = 100;
        $num2 = "50";
        $result = $num1 + $num2;
        //echo $result = $num1 + $num2; this works but is goofy syntax
        echo $result;
        $i  = 0;
        while($i < 7)
        {
            echo "<h$i>Hello World!</h>";
            $i++;
        }
        echo "<br>";

        $j = 6;
        do
        {
            echo "<h$j>Howdy Partner!</h>";
            $j--;
        }while($j > 0);

        for($count = 1; $count < 6; $count++)
        {
            echo "<h$count>I'm Falling!</h>";
        }

        echo "<br/><br/><hr/><br/>";
        $full_name = "Bobby Joe";
        echo "<h1>$full_name<h1>";
        echo "<br/><br/><hr/><br/>";
        // B o b b y   J o e
        // 0 1 2 3 4 5 6 7 8
        $position = strpos($full_name, ' ');
        echo"<h1>$position</h1>";

        echo strtolower($full_name);
        echo "<br/><br/><hr/><br/>";

        echo strtoupper($full_name);
        echo "<br/><br/><hr/><br/>";

        $name_parts = explode(' ', $full_name);
        echo "First Name: " . $name_parts[0];
        echo "<br/>";
        echo "Last Name: " . $name_parts[1];

        echo "<br/><br/><hr/><br/>";
        $string = ' testing ';
        echo trim($string);
        echo "<br/><br/><hr/><br/>";

        echo "<br/><br/><hr/><br/>";

    ?>

    <p></p>
</main>
<footer><?php include ('../includes/footer.php'); ?></footer>
</body>
</html>