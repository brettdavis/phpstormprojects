<?php
class Car
{
    //the make of the car
    public $make;
    //the model of the car
    public $model;
    //the color of the car
    public $color;
    //the year the car was made
    public $year;
    //the current state of the car. String (i.e. Forward, Reverse, Park)
    public $status;
    //the vehicle identification number
    private $vin;

    function __construct($vin, $status = 'park')
    {
        $this->status = $status;
        $this->vin = $vin;
    }

    public function forward()
    {
        $this->status = 'forward';
        echo "The car is moving forward!";
    }

    public function reverse()
    {
        $this->status = 'reverse';
        echo "The car is moving backwards!";
    }

    public function park()
    {
        $this->status = 'park';
        echo "The car is in park!";
    }

    public function getVIN()
    {
        return $this->vin . "<br><br>";
    }

    public function setVIN($new_vin)
    {
        $this->vin = $new_vin;
    }
}//--end of class
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>OOP Demo</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include('../includes/header.php'); ?></header>
<nav><?php include ('../includes/nav.php'); ?></nav>
<main>
    <h2>OOP Demo</h2>
    <p><?php
        $my_car = new Car('12345678910111');
        $my_car->make = 'Jeep';
        $my_car->model = 'Liberty';
        $my_car->color = 'Blue';
        $my_car->year = 2018;
        $my_car->forward();
        $my_car->setVIN('J1357542F1');
        $my_car->getVIN();

        echo"<pre>";
        print_r($my_car);
        ?>
    </p>
</main>
<footer><?php include ('../includes/footer.php'); ?></footer>
</body>
</html>
