<?php
session_start();
include_once ('../includes/dbConfig.php');
$error_msg = '';

//Checking for both the email and password
if(isset($_POST['userlogin'])
            && isset($_POST['email']) && !empty($_POST['email'])
            && isset($_POST['password']) && !empty($_POST['password'])) {

    $email = $_POST['email'];
    $submitted_password = $_POST['password'];

    //connect to the database
    $db = new PDO($dsn, $username, $password, $options);
    $sql = $db->prepare("
                          SELECT 
                            password, role_id, member_key 
                          FROM 
                            phpclass.member_login 
                          WHERE 
                            email = :Email");
    $sql->bindValue(':Email', $email);
    $sql->execute();
    $row = $sql->fetch();

    if ($row != null) {
        $hashed_password = md5($submitted_password . $row['member_key']);

        //admin check
        if ($hashed_password == $row['password'] && $row['role_id'] == 2) {
            $_SESSION['UID'] = $row['member_key'];
            $_SESSION['ROLE'] = $row['role_id'];
            header("Location:admin.php");
        }
        //member check
        elseif ($hashed_password == $row['password'] && ($row['role_id'] == 1 || $row['role_id'] == 3)) {
            $_SESSION['UID'] = $row['member_key'];
            $_SESSION['ROLE'] = $row['role_id'];
            header("Location:member.php");
        }
        else {
            $error_msg = 'Email and Password do not match!';
        }
    }
    else {
        $error_msg = 'Email and Password do not match!';
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Update Movie</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>
<header><?php include('../includes/header.php'); ?></header>
<nav><?php include ('../includes/nav.php'); ?></nav>
<main>
    <h1>Login</h1>

    <h3 class="error"><?= $error_msg;?></h3>
    <form method="post">
        <table class="table table-hover table-border"style="margin: auto;">
            <tr>
                <th>Email</th>
                <td><input type="text" name="email" value="" size="50" required></td>
            </tr>
            <tr>
                <th>Password</th>
                <td><input type="password" name="password" value="" size="50" required></td>
            </tr>
            <tr>
                <td colspan="2"><input type="submit" name="userlogin" value="Login"></td>
            </tr>
        </table>
    </form>
</main>
<footer><?php include ('../includes/footer.php'); ?></footer>
</body>
</html>