<?php
session_start();
include_once ('../includes/dbConfig.php');

if($_SESSION['ROLE'] != 2){
    header("Location:index.php");
    $error_msg = '';
}

$error_msg = '';

if(!isset($_SESSION['UID']))
{
    header("Location:index.php");
}

//adding new user
if(isset($_POST['addmember'])) {

    //validation checks
    if (empty($_POST['fullname'])) {
        $error_msg = 'Name is required';
    } else {
        $fullname = $_POST['fullname'];
    }

    if (empty($_POST['email'])) {
        $error_msg = 'Email is required';
    } else {
        $email = $_POST['email'];
    }

    if (empty($_POST['password1'])) {
        $error_msg = 'Password is required';
    } else {
        $pword1 = $_POST['password1'];
    }

    if (empty($_POST['password2'])) {
        $error_msg = 'Password verification is required';
    } else {
        $pword2 = $_POST['password2'];
    }

    if (empty($_POST['role'])) {
        $error_msg = 'Role is required';
    } else {
        $role = $_POST['role'];
    }

    if($pword1 != $pword2) {
        $error_msg = 'Passwords must match';
    }


    try{
        $db = new PDO($dsn, $username, $password, $options);
        $sql = $db->prepare("
                          SELECT 
                            member_id 
                            FROM 
                              phpclass.member_login
                              WHERE 
                                email = :Email");
        $sql->bindValue(':Email', $email);
        $sql->execute();
        $row = $sql->fetch();
        if($row != null){
            $error_msg = $email . " already exists as an account.";
        }
    }
    catch(PDOException $e){
        $error = $e->getMessage();
        echo "Error: ".$error;
        exit();
    }

    //do some db stuff
    if (empty($error_msg)){
        $member_key = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));

        try {
            $db = new PDO($dsn, $username, $password, $options);
            $sql = $db->prepare("
                              INSERT INTO phpclass.member_login
                              (name, email, password, role_id, member_key) 
                              VALUE 
                              (:Name , :Email, :Password, :Roleid, :Memberkey)");
            $sql->bindValue(':Name', $fullname);
            $sql->bindValue(':Email', $email);
            $sql->bindValue(':Password', md5($pword2 . $member_key));
            $sql->bindValue(':Roleid', $role);
            $sql->bindValue(':Memberkey', $member_key);
            $sql->execute();

            $error_msg = 'New member added!';
            unset($fullname, $email, $pword1, $pword2, $role);
        }
        catch (PDOException $e) {
            echo "DB Error: " . $e->getMessage();
            exit;
        }
    }
}
//dynamic dropdown menu
try{
    $db = new PDO($dsn, $username, $password, $options);
    $sql = $db->prepare("
                          SELECT 
                            role_id, role_value
                            FROM 
                              phpclass.role");
    $sql->execute();
    $rows = $sql->fetchAll();
}
catch(PDOException $e){
    $error = $e->getMessage();
    echo "Error: " . $error;
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Admin</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>
<header><?php include('../includes/header.php'); ?></header>
<nav><?php include ('../includes/nav.php'); ?></nav>
<main>
    <h1>Admin Page</h1>
    <h3 class="error"><?= $error_msg?></h3>
    <form action="" method="post">
        <table class="table table-hover table-border"style="margin: auto;">
            <tr>
                <th colspan="2"><h2>Add New Member</h2></th>
            </tr>
            <tr>
                <th>Full Name</th>
                <td><input type="text" name="fullname" value="<?= $fullname?>" size="50"></td>
            </tr>
            <tr>
                <th>Email Address</th>
                <td><input type="email" name="email" value="<?= $email?>" size="50"></td>
            </tr>
            <tr>
                <th>Password</th>
                <td><input type="password" name="password1" value="<?= $pword1?>" size="50"></td>
            </tr>
            <tr>
                <th>Confirm Password</th>
                <td><input type="password" name="password2" value="" size="50"></td>
            </tr>
            <tr>
                <th>Role</th>
                <td>
                    <select name="role">
                        <?php foreach($rows as $row): ?>
                        <option value="<?= $row['role_id']?>"><?= $row['role_value']?></option>
                        <?php endforeach;?>
                    </select>
                </td>
            </tr>
            <tr>
                <th colspan="2"><input type="submit" name="addmember" value="Add"></th>
            </tr>
        </table>
    </form>
</main>
<footer><?php include ('../includes/footer.php'); ?></footer>
</body>
</html>