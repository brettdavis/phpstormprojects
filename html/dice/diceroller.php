<?php

for($i = 0; $i < 6; $i++)
{
    $dice_val[$i] = mt_rand(1,6);
    $dice_image[$i] = '/images/dice_'.($dice_val[$i]).'.png';
}
$player_score = $dice_val[0]+$dice_val[1]+$dice_val[2];
$computer_score = $dice_val[3]+$dice_val[4]+$dice_val[5];

if($player_score == $computer_score) $result = 'It\'s a tie';
elseif($player_score > $computer_score) $result = 'You win!';
elseif($computer_score > $player_score) $result = 'Computer wins!';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Dice Roller</title>
    <link rel="stylesheet" type="text/css" href="/css/base.css">
</head>
<body>
<header><?php include('../includes/header.php'); ?></header>
<nav><?php include ('../includes/nav.php'); ?></nav>
<main>
    <h2>Dice Roller</h2>
    <form action ="" method="post">
        <h4>Your Score: <?=$player_score?></h4><br>
        <div class="dice">
        <img  src= "<?=$dice_image[0]?>"/>
        <img  src= "<?=$dice_image[1]?>"/>
        <img  src= "<?=$dice_image[2]?>"/><br>
        <h4>Computer's Score: <?=$computer_score?></h4><br>
        <img  src= "<?=$dice_image[3]?>"/>
        <img  src= "<?=$dice_image[4]?>"/>
        <img  src= "<?=$dice_image[5]?>"/><br>
        </div>
        <h3>Result: <?=$result?></h3>
        <input type="submit" value="Roll again"/>
    </form>
</main>
<footer><?php include ('../includes/footer.php'); ?></footer>
</body>
</html>
