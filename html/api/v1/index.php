<?php
//require the Slim framework and autoload the framework
require "Slim/Slim.php";
\Slim\Slim::registerAutoloader();

/*
 * http method = Database Statement
 * put = update
 * post = insert
 * get = select
 * delete = delete
 */

//instantiate the slim framework
$app = new \Slim\Slim();

//routes
/*$app->get('/getHello', 'getHello');
$app->get('/showMember/:memberName', 'showMember');
$app->post('/addMember/:memberName', 'addMember');
$app->post('/addJson', 'addJson');
$app->post('/getMemberAddress', 'getMemberAddress');
$app->post('/getMemberName', 'getMemberName');
$app->delete('/delMember/:userId', 'delMember');*/

//assignment routes
$app->get('/get_races', 'getRaces');
$app->get('/get_runners/:race_id', 'getRunners');
$app->post('/add_runner', 'addRunner');
$app->delete('/delete_runner', 'deleteRunner');
$app->put('/update_runner', 'updateRunner');

$app->run();

//assignment route functions
function getRaces(){
    include_once ('../../includes/dbConfig.php');
    try{
        $db = new PDO($dsn, $username, $password, $options);
        $sql = $db->prepare("
          SELECT 
            * 
          FROM 
            phpclass.race");
        $sql->execute();
        $rows = $sql->fetchAll();
        $result['races'] = $rows;
        echo json_encode($result);
    }catch(PDOException $e){
        $error = $e->getMessage();
        $errors['error'] = $error;
        echo json_encode($errors);
    }
}

function getRunners($race_id){
    include_once ('../../includes/dbConfig.php');
    try{
        $db = new PDO($dsn, $username, $password, $options);
        $sql = $db->prepare("
          SELECT
            phpclass.member_login.`name`,
            phpclass.member_login.email,
            phpclass.member_race.race_id
          FROM
            phpclass.member_login
          INNER JOIN phpclass.member_race 
            ON phpclass.member_login.member_id = phpclass.member_race.member_id
          WHERE
            phpclass.member_race.race_id = :RaceId");
        $sql->bindValue(':RaceId', $race_id);
        $sql->execute();
        $rows = $sql->fetchAll();
        $result['runners'] = $rows;
        echo json_encode($result);
    }catch(PDOException $e){
        $error = $e->getMessage();
        $errors['error'] = $error;
        echo json_encode($errors);
    }
}

function addRunner(){
    $request = \Slim\Slim::getInstance()->request();
    $post_json = $request->getBody(); //{"member_id":"10","race_id":"1", "member_key":"12345"}
    $post_data = json_decode($post_json, TRUE);
    $member_id = $post_data['member_id'];
    $race_id = $post_data['race_id'];
    $member_key = $post_data['member_key'];

    //need to make sure the member key is valid (belongs to a promoter)
    include_once ('../../includes/dbConfig.php');
    try{
        $db = new PDO($dsn, $username, $password, $options);
        $sql = $db->prepare(" 
          SELECT
            phpclass.member_race.race_id
          FROM
            phpclass.member_race
          INNER JOIN phpclass.member_login 
            ON phpclass.member_race.member_id = phpclass.member_login.member_id
          WHERE
            phpclass.member_login.member_key = :MemberKey AND
            phpclass.member_race.role_id = 3");
        $sql->bindValue(':MemberKey', $member_key);
        $sql->execute();
        $row = $sql->fetch();

        if($row != null){
            $sql = $db->prepare("
              INSERT INTO phpclass.member_race
              (member_id, race_id, role_id) 
              VALUES (:MemberId, :RaceId, 1)");
            $sql->bindValue(':MemberId', $member_id);
            $sql->bindValue(':RaceId', $race_id);
            $sql->execute();
        }else{
            echo "BAD API KEY";
        }

    }catch(PDOException $e){
        $error = $e->getMessage();
        $errors['error'] = $error;
        echo json_encode($errors);
    }
}

function deleteRunner(){
    $request = \Slim\Slim::getInstance()->request();
    $post_json = $request->getBody(); //{"member_id":"10", "member_key":"12345"}
    $post_data = json_decode($post_json, TRUE);
    $member_id = $post_data['member_id'];
    $member_key = $post_data['member_key'];

    //need to make sure the member key is valid (belongs to a promoter)
    include_once ('../../includes/dbConfig.php');
    try{
        $db = new PDO($dsn, $username, $password, $options);
        $sql = $db->prepare(" 
          SELECT
            phpclass.member_race.race_id
          FROM
            phpclass.member_race
          INNER JOIN phpclass.member_login 
            ON phpclass.member_race.member_id = phpclass.member_login.member_id
          WHERE
            phpclass.member_login.member_key = :MemberKey AND
            phpclass.member_race.role_id = 3");
        $sql->bindValue(':MemberKey', $member_key);
        $sql->execute();
        $row = $sql->fetch();

        if($row != null){
            $sql = $db->prepare("
              DELETE FROM phpclass.member_race
              WHERE member_id = :MemberId");
            $sql->bindValue(':MemberId', $member_id);
            //$sql->bindValue(':RaceId', $race_id);
            $sql->execute();
        }else{
            echo "BAD API KEY";
        }

    }catch(PDOException $e){
        $error = $e->getMessage();
        $errors['error'] = $error;
        echo json_encode($errors);
    }
}

function updateRunner(){
    $request = \Slim\Slim::getInstance()->request();
    $post_json = $request->getBody(); //{"test":"test"}
    $post_data = json_decode($post_json, TRUE);
    echo "Updated runner " . $post_data['test'];
}

//route functions
/*function getHello(){
    echo "Howdy World!";
}

function showMember($memberName){
    echo "Hello $memberName";
}

function addMember($memberName){
    echo "Adding Member $memberName";
}

function addJson(){
    $request = \Slim\Slim::getInstance()->request();
    $post_json = $request->getBody(); //{"test":"test"}
    $post_data = json_decode($post_json,TRUE);
    echo $post_data['address'];
}

function getMemberAddress(){
    $request = \Slim\Slim::getInstance()->request();
    $post_json = $request->getBody(); //{"test":"test"}
    $post_data = json_decode($post_json,TRUE);
    echo $post_data['address'];
}

function getMemberName()
{
    $request = \Slim\Slim::getInstance()->request();
    $post_json = $request->getBody(); //{"test":"test"}
    $post_data = json_decode($post_json, TRUE);
    echo $post_data['fname'] . " " . $post_data['lname'];
}

function delMember($userId){
    echo "Deleting $userId";
}*/

