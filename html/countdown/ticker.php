<?php
    $SEC_PER_MIN = 60;
    $SEC_PER_HR = 60 * $SEC_PER_MIN;
    $SEC_PER_DAY = 24 * $SEC_PER_HR;
    $SEC_PER_YR = 365 * $SEC_PER_DAY;

    $NOW = time();
    //This specifies the day that the semester ends (May 16, 2020)
    $END_OF_SEMESTER = mktime(0,0,0,5,17,2020);
    //Calculating differences between now and the end of semester
    $seconds = $END_OF_SEMESTER - $NOW;
    $years = floor($seconds/$SEC_PER_YR);
    //Recalculate total seconds remaining till our date, round down to get the next lowest amount of time, rinse and repeat...
    $seconds = $seconds - ($SEC_PER_YR * $years);
    $days = floor($seconds/$SEC_PER_DAY);

    $seconds = $seconds - ($SEC_PER_DAY  * $days);
    $hours = floor($seconds/$SEC_PER_HR);

    $seconds = $seconds - ($SEC_PER_HR * $hours);
    $minutes = floor($seconds/$SEC_PER_MIN);

    $seconds = $seconds - ($SEC_PER_MIN * $minutes);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Countdown</title>
    <link rel="stylesheet" type="text/css" href="../css/base.css">
</head>
<body>
<header><?php include('../includes/header.php'); ?></header>
<nav><?php include ('../includes/nav.php'); ?></nav>
<main style="text-align: center;">
    <h1>Countdown Demo</h1>
    <!--<h3>Now: <?= $NOW; ?> seconds</h3>
    <h3>End of Semester: <?= $END_OF_SEMESTER; ?> seconds</h3>
    <h3>Total Diff: <?= $seconds; ?> seconds</h3>
    <h3>Years: <?= $years; ?> years</h3>-->
    <h3>Days: <?= $days; ?> days</h3>
    <h3>Hours: <?= $hours; ?> hours</h3>
    <h3>Minutes: <?= $minutes; ?> minutes</h3>
    <h3>Seconds: <?= $seconds; ?> seconds</h3>
    <p></p>
</main>
<footer><?php include ('../includes/footer.php'); ?></footer>
</body>
</html>